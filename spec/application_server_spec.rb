require 'spec_helper'

describe 'ApplicationServer' do
  include Rack::Test::Methods

  let :app do
    ApplicationServer.to_app
  end

  describe 'GET /v1/docs (Swagger Docs JSON)' do
    describe 'unauthorized' do
      before { get '/v1/docs' }

      it 'returns 401' do
        expect(last_response.status).to eq 401
      end

      it 'returns text/plain' do
        expect(last_response.headers['Content-Type']).to eq 'text/plain'
      end
    end

    describe 'authorized' do
      before do
        basic_authorize 'carrot', '329carrot'
        get '/v1/docs'
      end

      it 'returns 200' do
        expect(last_response.status).to eq 200
      end

      it 'returns application/json' do
        expect(last_response.headers['Content-Type']).to eq 'application/json'
      end
    end
  end

  describe 'GET / (Swagger UI)' do
    before { get '/' }

    it 'returns 200' do
      expect(last_response.status).to eq 200
    end

    it 'returns text/html' do
      expect(last_response.headers['Content-Type']).to eq 'text/html'
    end
  end
end
