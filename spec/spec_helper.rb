$LOAD_PATH.unshift("#{File.dirname(__FILE__)}/..")
ENV['RACK_ENV'] = 'test'

require 'rspec'
require 'rack/test'
require 'airborne'
require 'simplecov'
require 'coveralls'

Coveralls.wear!

SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter[
  Coveralls::SimpleCov::Formatter,
  SimpleCov::Formatter::HTMLFormatter
]

SimpleCov.start do
  add_group 'API V1', 'api/v1'
  add_group 'Models', 'models'
  add_filter '/spec/'
  add_filter 'unicorn.rb'
end

require File.expand_path('../../application', __FILE__)

Airborne.configure do |config|
  config.rack_app = API::Base
end
