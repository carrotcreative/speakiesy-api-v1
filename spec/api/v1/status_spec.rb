require 'spec_helper'

describe API::Base do
  describe 'GET /v1/status' do
    it 'returns 200' do
      get '/v1/status'
      expect_status(200)
    end

    it 'returns that the API status' do
      get '/v1/status'
      expect_json(status: 'available')
    end
  end
end
