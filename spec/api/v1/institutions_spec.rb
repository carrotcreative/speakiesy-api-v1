require 'spec_helper'

describe API::V1::Institutions do
  describe 'GET /v1/institutions/search?query=%s' do
    describe 'correct params' do
      before { get '/v1/institutions/search?query=temple.edu' }

      it 'returns 200' do
        expect_status(200)
      end

      it 'returns query' do
        expect_json(query: 'temple.edu')
      end
    end

    describe 'incorrect params' do
      context 'missing' do
        before { get '/v1/institutions/search' }

        it 'returns 400' do
          expect_status(400)
        end

        it 'returns error' do
          expect_json(error: 'query is missing')
        end
      end

      context 'formatting' do
        it 'returns error on blank input' do
          get '/v1/institutions/search?query='

          expect_status(400)
          expect_json(error: 'query is invalid')
        end

        it 'returns error on `templeedu`' do
          get '/v1/institutions/search?query=templeedu'

          expect_status(400)
          expect_json(error: 'query is invalid')
        end
      end
    end
  end
end
