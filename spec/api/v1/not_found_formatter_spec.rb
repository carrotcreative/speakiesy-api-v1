require 'spec_helper'

describe API::V1 do
  describe '404 error' do
    it 'returns 404' do
      get '/v1/foo/bar'
      expect_status(404)
    end

    it 'returns as JSON' do
      get '/v1/foo/bar'
      expect_header('Content-Type', 'application/json')
    end

    it 'returns an error message' do
      get '/v1/foo/bar'
      expect_json(error: 'not found')
    end
  end
end
