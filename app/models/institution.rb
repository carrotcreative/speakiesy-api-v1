# -- Model for Institutions
class Institution < ActiveRecord::Base
  attr_accessible

  # rubocop:disable Metrics/LineLength
  class Entity < Grape::Entity
    format_with(:iso_timestamp) { |dt| dt.iso8601 }

    expose :uid, documentation: { type: 'string', desc: 'unique identifier' }
    expose :name, documentation: { type: 'string', desc: 'institution name' }
    expose :active, documentation: { type: 'boolean', desc: 'active institution?' }
    expose :address, documentation: { type: 'string', desc: 'institution address' }
    expose :phone_number, documentation: { type: 'string', desc: 'institution phone number' }
    expose :campus_abbreviation, documentation: { type: 'string', desc: 'institution campus abbreviation' }
    expose :email_domain_type, documentation: { type: 'integer', desc: 'email domain type' }
    expose :institution_type, documentation: { type: 'integer', desc: 'institution type' }
    expose :user_count, documentation: { type: 'integer', desc: 'number of users at this institution' }

    with_options(format_with: :iso_timestamp) do
      expose :created_at
      expose :updated_at
    end
  end
  # rubocop:enable Metrics/LineLength
end
