Dir["#{File.dirname(__FILE__)}/v1/**/*.rb"].each { |f| require f }

module API
  module V1
    # V1 API Class
    #
    # - Defines global settings and endpoints for v1
    # - Mounts all versions of v1 API
    class Base < Grape::API
      version 'v1', using: :path

      mount API::V1::Auth
      mount API::V1::Institutions
      mount API::V1::Posts
      mount API::V1::Users
      mount API::V1::Status

      add_swagger_documentation hide_format: false,
                                base_path: '/',
                                api_version: 'v1',
                                mount_path: '/docs',
                                hide_documentation_path: true
    end
  end
end
