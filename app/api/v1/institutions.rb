module API
  module V1
    # -- Institution endpoints
    class Institutions < Grape::API
      namespace :institutions do
        desc 'Update an institution' do
          success API::V1::Entities::Status
          failure [[400, 'invalid param']]
        end
        params do
          requires :uid, type: String,
                         desc: 'UUID of institution'
        end
        put ':uid' do
          status 201
          # present Institution.new, with: Institution::Entity
        end

        desc 'Search by institution domain' do
          success Institution::Entity
          failure [[400, 'invalid param']]
        end
        params do
          requires :query, type: String,
                           desc: 'Domain of email address',
                           regexp: /^(.*?@)?[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/ # rubocop:disable Metrics/LineLength
        end
        get :search do
          # institution = Institutions.search(params[:query])
          # present institution, with: Institution::Entity

          { query: params[:query] }
        end
      end
    end
  end
end
