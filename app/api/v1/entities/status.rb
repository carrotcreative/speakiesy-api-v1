module API
  module V1
    module Entities
      # rubocop:disable Metrics/LineLength
      class Status < Grape::Entity
        expose :status, documentation: { type: 'string', desc: 'response status', required: true }
        expose :details, documentation: { type: 'string', desc: 'response details' }
      end
      # rubocop:enable Metrics/LineLength
    end
  end
end
