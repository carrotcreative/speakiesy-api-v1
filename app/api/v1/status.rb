module API
  module V1
    # -- API status endpoints
    class Status < Grape::API
      namespace :status do
        get do
          { status: 'available' }
        end
      end
    end
  end
end
