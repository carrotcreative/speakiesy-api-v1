require 'app/api/v1'

module API
  # Base API Class
  #
  # - Defines global settings and endpoints
  # - Mounts all versions of API
  class Base < Grape::API
    format :json

    mount API::V1::Base

    route :any, '*path' do
      error!('not found', 404)
    end
  end
end
