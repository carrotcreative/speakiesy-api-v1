# [DEPRECIATED] Speakiesy API
[![Build Status](https://magnum.travis-ci.com/carrot/speakiesy-api.svg?token=uzeqY6NiW9AmRiLNAKF4&branch=master)](https://magnum.travis-ci.com/carrot/speakiesy-api)
[![Coverage Status](https://coveralls.io/repos/carrot/speakiesy-api/badge.svg?branch=master&t=6YSf8E)](https://coveralls.io/r/carrot/speakiesy-api?branch=master)

Carrot's version of the Speakiesy API

### Interactive Documentation

[Documentation](http://speakiesy-api-staging.herokuapp.com) Username/Password Required

### Command Line

```
// Print Grape routes
rake grape:routes

// Run specs
rspec

// Validate code style conformity
rubocop . -a

// Code quality reporting
rubycritic

// Check for security flaws. (Take with a grain of salt as this was created specifically for Rails)
brakeman

// Audit gems used via Bundler
bundle exec bundle-audit
```

### Resources

- [Proposed API Interface](docs/endpoints.md)
- [Existing API Interface](docs/existing_endpoints.md) ([Example Responses](docs/responses))
