require File.expand_path('../environment', __FILE__)

# -- API module instantiation
module API; end

# -- Include models
Dir["#{File.dirname(__FILE__)}/app/models/**/*.rb"].each { |f| require f }

# -- Require API base
require 'app/api/base'

# -- Enable mass assignment security
ActiveRecord::Base.instance_eval do
  include ActiveModel::MassAssignmentSecurity
  attr_accessible []
end

# -- Basic authentication for documentation
class DocsAuth < Rack::Auth::Basic
  def call(env)
    if %r{\/docs(\/.*|\z)}.match(Rack::Request.new(env).path)
      super # perform auth
    else
      @app.call(env) # pass-thru
    end
  end
end

# -- Build the Rack application
ApplicationServer = Rack::Builder.new do
  # -- Authenticate Swagger documentation
  use DocsAuth, '' do |username, password|
    [username, password] == [ENV['DOCS_USERNAME'], ENV['DOCS_PASSWORD']]
  end

  # -- Serve static files (Swagger UI)
  use Rack::Static, urls: [
    '/images',
    '/css',
    '/fonts',
    '/lib',
    '/swagger-ui.js',
    '/o2c.html'
  ], root: 'public/docs', index: 'index.html'

  # -- Run API
  map '/' do
    run API::Base
  end
end
