$LOAD_PATH.unshift File.dirname(__FILE__)
env = (ENV['RACK_ENV'] || :development)

require 'bundler'
Bundler.require :default, env.to_sym
require 'erb'

# -- Application module instantiation
module Application
  include ActiveSupport::Configurable
end

Application.configure do |config|
  config.root = File.dirname(__FILE__)
  config.env = ActiveSupport::StringInquirer.new(env.to_s)
end

# -- Load dotenv environment variables
env_file = ".env.#{ENV['RACK_ENV']}"
Dotenv.load(env_file) if File.exist?(env_file)

# -- Load DB config & estabilish connection
db_config = YAML.load(ERB.new(File.read('config/database.yml')).result)
db_config = db_config[Application.config.env]

ActiveRecord::Base.default_timezone = :utc
ActiveRecord::Base.establish_connection(db_config)

# -- Load environment specific file
specific_environment = "config/environments/#{Application.config.env}.rb"
require specific_environment if File.exist? specific_environment
Dir['config/initializers/**/*.rb'].each { |f| require f }
