# -- Gem source
source 'https://rubygems.org'

# -- Ruby version
ruby '2.2.2'

# -- Frameworks
gem 'grape', '~> 0.12.0'
gem 'grape-entity', '~> 0.4.5'
gem 'grape_logging', '~> 1.0.3'
gem 'grape-swagger'

# -- Data Stores
gem 'mysql2', '~> 0.3.18'
gem 'activerecord', '~> 4.2.0'
gem 'protected_attributes', '~>1.1.0'
gem 'activeuuid', '~> 0.6.0'

# -- Testing
gem 'rspec', '~> 3.3.0', require: false, group: :test
gem 'airborne', '~> 0.1.15', require: false, group: :test
gem 'coveralls', '~> 0.8.1', require: false, group: :test

# -- Static Analysis
gem 'rubocop', '~> 0.32.0', require: false, group: :development
gem 'brakeman', require: false, group: :development
gem 'rubycritic', require: false, group: :development
gem 'bundler-audit', require: false, group: :development

# -- System
gem 'foreman', '~> 0.78.0', require: false
gem 'dotenv', '~> 2.0.2'
gem 'unicorn', '~> 4.9.0'
gem 'rake', '~> 10.4.2'
