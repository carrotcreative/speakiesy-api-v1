## Initial Proposed Interface

### Notes:

*These are subject to change.*

### Format:

New -> Old

### USER
```
[POST]   /users                 -> [POST]   /profile/new
[GET]    /users/1               -> [GET]    /userProfile
[PUT]    /users/1               -> [PUT]    /profile/avatar
[POST]   /users/1/password      -> [POST]   /changePassword

[PUT]    /users/1/activate      -> [PUT]    /students/1/activate
[POST]   /users/1/ban           -> [PUT]    /students/1/ban
[DELETE] /users/1/ban           -> [PUT]    /students/1/releaseBan

[POST]   /users/1/follow        -> [POST]   /profiles/{id}/follow
[DELETE] /users/1/follow        -> [DELETE] /profiles/{id}/unfollow

[POST]   /user/1/inappropriate  -> [POST]   /profiles/{id}/inappropriate
[DELETE] /user/1/inappropriate  -> [DELETE] /profiles/{id}/inappropriate

[GET]    /user/1/feed           -> [GET]    /feed/my
[GET]    /user/1/feed/following -> [GET]    /feed/followings
```

### GENERAL FEED
```
[GET] /feed -> [GET] /feed
```

### POSTS
```
[GET]    /posts                 -> [GET]    /posts/popular
[POST]   /posts                 -> [POST]   /posts
[POST]   /posts/1/like          -> [POST]   /posts/1/like
[DELETE] /posts/1/like          -> [POST]   /posts/1/dislike
[POST]   /posts/1/inappropriate -> [POST]   /posts/1/inappropriate
[DELETE] /posts/1/inappropriate -> [DELETE] /posts/1/inappropriate
```

### COMMENTS
```
[GET]    /posts/1/comments                 -> ?
[POST]   /posts/1/comments                 -> [GET]    /posts/{postId}/comments
[POST]   /posts/1/comments/2/like          -> [POST]   /comments/{id}/like
[DELETE] /posts/1/comments/2/like          -> [POST]   /comments/{id}/dislike
[POST]   /posts/1/comments/2/inappropriate -> [POST]   /comments/{id}/inappropriate
[DELETE] /posts/1/comments/2/inappropriate -> [DELETE] /comments/{id}/inappropriate
```

### AUTHENTICATION
```
[POST] /auth/login            -> [POST] /login
[POST] /auth/logout           -> [POST] /logout
[POST] /auth/restore_password -> [POST] /restorePassword
[POST] /auth/verify           -> [POST] /verifyAccount
```

### UNIVERSITIES/INSTITUTIONS
```
[GET] /institutions/search/temple.edu -> [GET] /universities/checkDomainName
[PUT] /institutions/1                 -> [PUT] /universities/{id}/updateUniversity
```

### MEDIA
```
? -> [GET]    /media/{id}.{ext}
? -> [DELETE] /media/{id}
```

### MISC
```
[GET] /menu -> [GET] /menu
```

### UNASSIGNED FROM EXISTING API
```
[POST] /register                      -> Combined with [POST] /users
[POST] /reverifyAccount               -> Combined with [POST] /auth/verify
[DELETE] /profiles/{id}/inappropriate -> Combined with [PUT]  /users/1
```
