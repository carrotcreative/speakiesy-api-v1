# Speakiesy API - v1 Docs

> **Note:** these docs are being put together by [carrot](https://github.com/carrot) by reverse engineering the previous API source code. There was no provided API spec/docs, so please forgive any incompleteness or omissions

## API endpoints
All routes follow this base `/api/v1/`

- `/menu` - GET
- `/userProfile` - GET
- `/profile/new` - POST
- `/profile/avatar` - PUT
- `/changePassword` - POST
- `/login` - POST
- `/logout` - POST
- `/register` - POST
- `/restorePassword` - POST
- `/verifyAccount` - POST
- `/reverifyAccount` - POST
- `/students/{id}/{action}` - PUT - name: "ActivateAccount"
- `/students/{id}/{action}` - PUT - name: "Ban"
- `/students/{id}/{action}` - PUT - name: "ReleaseBan"
- `/universities/checkDomainName` - GET - name: 'GetUniversitiesByDomainName'
- `/universities/{id}/updateUniversity` - PUT - name: 'updateUniversity'
- `/posts` - POST - name: 'PostPosts'
- `/feed` - GET - name: 'GetFeed'
- `/feed/followings` - GET - name: 'GetFollowingsFeed'
- `/feed/my` - GET - name: 'GetMyFeed'
- `/posts/popular` - GET - name: 'GetPopular'
- `/posts/{postId}/comments` - GET - name: 'GetComments'
- `/media/{id}.{ext}` - GET - name: 'GetMedia'
- `/media/{id}` - DELETE - name 'DeleteMedia'
- `/profiles/{id}/follow` - POST - name: 'Follow'
- `/profiles/{id}/unfollow` - DELETE - name: 'Unfollow'
- `/profiles/{id}/inappropriate` - POST - name 'InappropriateAvatar'
- `/profiles/{id}/inappropriate` - DELETE - name 'DeleteInappropriateAvatar'
- `/posts/{id}/like` - POST - name: 'Like'
- `/posts/{id}/dislike` - POST - name: 'PostDislike'
- `/posts/{id}/inappropriate` - POST - name: 'PostInappropriate'
- `/posts/{id}/like` - DELETE - name: 'PostDeleteLike'
- `/posts/{id}/dislike` - DELETE - name: 'PostDeleteDislike'
- `/posts/{id}/inappropriate` - DELETE - name: 'PostDeleteInappropriate'
- `/comments/{id}/like` - POST - name: 'CommentLike'
- `/comments/{id}/inappropriate` - POST - name: 'CommentInappropriate'
- `/comments/{id}/like` - DELETE - name: 'CommentDeleteLike'
- `/comments/{id}/dislike` - DELETE - name: 'CommentDeleteDislike'
- `/comments/{id}/inappropriate` - DELETE - name: 'CommentDeleteInappropriate'

