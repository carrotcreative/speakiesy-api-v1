# Device Token

- Source: [source/Domain/DeviceToken.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/DeviceToken.cs)
- Inherits: [source/Domain/UUIDEntity.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/UUIDEntity.cs)

| Field  	                | Type            | Required  | Other                         |
|-----------------------	|----------------	|----------	|-----------------------------	|
| UUID                    | String(32)      |           |                               |
| StudentId               | Long            |           | [ForeignKey("Student")]       |
| Student                 | Student         |           | [ForeignKey("StudentId")]     |
| AppId                   | Int             |           | [ForeignKey("App")]           |
| App                     | App             |           | [ForeignKey("AppId")]         |
| Token                   | String(255)     |           |                               |
