# Comment

- Source: [source/Domain/Comment.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/Comment.cs)
- Inherits: [source/Domain/UUIDEntity.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/UUIDEntity.cs)

| Field  	                | Type            | Required  | Other                         |
|-----------------------	|----------------	|----------	|-----------------------------	|
| UUID                    | String(32)      |           |                               |
| Content                 | String(4000)    | Yes       |                               |
| Date                    | DateTime        | Yes       |                               |
| OrderNumber             | Long?           |           | NotMapped                     |
| LikedByMe               | Bool            |           | NotMapped                     |
| DislikedByMe            | Bool            |           | NotMapped                     |
| InappropriateByMe       | Bool            |           | NotMapped                     |
| ProfileId               | Long            | Yes       | [ForeignKey("Profile")]       |
| Profile                 | Profile         |           | [ForeignKey("ProfileId")]     |
| PostId                  | Long            | Yes       | [ForeignKey("Post")]          |
| Post                    | Post            |           | [ForeignKey("PostId")]        |
| CommentCounter          | CommentCounter  |           | [InverseProperty("Comment")]  |
