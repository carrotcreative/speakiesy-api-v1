# Advertiser

Table Name: `Advertisers`

- Source: [source/Domain/Advertiser.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/Advertiser.cs)
- Inherits: [source/Domain/User.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/User.cs)
- Inherits: [source/Domain/UUIDEntity.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/UUIDEntity.cs)

| Field  	                | Type          | Required  | Other                     |
|-----------------------	|--------------	|----------	|-------------------------	|
| UUID                    | String(32)    |           |                           |
| Email      	            | String(255)   | Yes      	| Unique Index      	      |
| Password               	| String(255)   |         	|       	                  |
| AccountStatus           | AccountStatus |         	|                         	|
| NeedChangePassword      | Bool    	    | Yes      	|                         	|
| AccountConfirmationKey  | String(255)   | Yes      	|                         	|
| RoleId                	| Long    	    | Yes      	| ForeignKey("Role")      	|
| Role                  	| Role  	      | Yes      	| ForeignKey("Role")      	|
| Profiles              	| ICollection   |         	| [InverseProperty("User")  |

