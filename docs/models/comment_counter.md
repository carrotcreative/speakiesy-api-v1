# Comment Counter

- Source: [source/Domain/Comment.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/Comment.cs)
- Inherits: [source/Domain/UUIDEntity.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/UUIDEntity.cs)
- Inherits: [source/Domain/ICounter.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/ICounter.cs)

| Field  	                | Type            | Required  | Other                         |
|-----------------------	|----------------	|----------	|-----------------------------	|
| UUID                    | String(32)      |           |                               |
| ID                      | Long            | Yes       | [ForeignKey("Comment")]       |
| Comment                 | Comment         |           | [ForeignKey("Id")]            |
| LikesCount              | Int             | Yes       |                               |
| DislikesCount           | Int             | Yes       |                               |
| InappropriatesCount     | Int             | Yes       |                               |
