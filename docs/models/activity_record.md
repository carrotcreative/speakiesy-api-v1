# Activity Record

- Source: [source/Domain/ActivityRecord.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/ActivityRecord.cs)
- Inherits: [source/Domain/PersistentEntity.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/PersistentEntity.cs)
- Inherits: [source/Domain/UUIDEntity.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/UUIDEntity.cs)

| Field  	                | Type          | Required  | Other                     |
|-----------------------	|--------------	|----------	|-------------------------	|
| UUID                    | String(32)    |           |                           |
| WhoId      	            | Long          | Yes      	| Unique Index      	      |
| Who                   	| Profile       |         	|       	                  |
| What                    | ActivityType  | Yes       |                         	|
| WhoseId                 | Long?         |         	|                         	|
| Whose                   | Profile       |         	|                         	|
| Date                    | DateTime      | Yes       |                         	|
| CommentId               | Long?         |         	|                         	|
| Comment                 | Comment       |         	|                         	|
| PostId                  | Long?         |         	|                         	|
| Post                    | Post          |         	|                         	|
| AppId                   | Long?         |         	|                         	|
| App                     | App           |         	|                         	|
| Deleted                 | Bool          | Yes       |                           |
