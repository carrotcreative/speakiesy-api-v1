# University

- Source: [source/Domain/University.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/University.cs)
- Inherits: [source/Domain/PersistentEntity.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/PersistentEntity.cs)
- Inherits: [source/Domain/UUIDEntity.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/UUIDEntity.cs)

| Field  	            | Type              | Required  | Other   |
|-------------------	|-----------------	|----------	|-------	|
| UUID                | String(32)        |           |         |
| Name      	        | String(255)     	| Yes      	|       	|
| Address           	| String(255)    	  |         	|       	|
| PhoneNumber        	| String(40)      	|         	|       	|
| Active      	      | Bool    	        | Yes      	|       	|
| HasBeenActivated  	| Bool    	        | Yes      	|       	|
| CampusAbbreviation 	| String(50)    	  | Yes      	|       	|
| UniversityType    	| UniversityType  	| Yes      	|       	|
| UniversityCounter  	| UniversityCounter |         	|       	|
| DomainNames      	  | ICollection     	|         	|       	|
| Students      	    | ICollection     	|         	|       	|
| Editors      	      | ICollection     	|         	|       	|
| ProfileUniversities | ICollection     	|         	|       	|
| Deleted             | Bool              | Yes       |         |

