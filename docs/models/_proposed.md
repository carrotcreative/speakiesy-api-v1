# User

- Source: [source/Domain/User.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/User.cs)
- Inherits: [source/Domain/UUIDEntity.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/UUIDEntity.cs)

| Field  	                | Type          | Required  | Other                     |
|-----------------------	|--------------	|----------	|-------------------------	|
| UUID                    | String(32)    |           |                           |
| Email      	            | String(255)   | Yes      	| Unique Index      	      |
| Password               	| String(255)   |         	|       	                  |
| AccountStatus           | AccountStatus |         	|                         	|
| NeedChangePassword      | Bool    	    | Yes      	|                         	|
| AccountConfirmationKey  | String(255)   | Yes      	|                         	|
| RoleId                	| Long    	    | Yes      	| ForeignKey("Role")      	|
| Role                  	| Role  	      | Yes      	| ForeignKey("Role")      	|
| Profiles              	| ICollection   |         	| [InverseProperty("User")  |



User
 - Administrator
 - Advertiser
 - Editor
 - Profile 
 - Profile Counter *
 - Profile Settings *
 - Student




User -  Global

| Field              | Type        | Details                           |
|------------------- |------------ |---------------------------------- |
| uuid               | String(32)  | PK                                |
| email              | String(255) | Required; Not Null; Unique Index  |
| password           | String(255) | Not Null                          |
| account_status     | Enum        | :not_verified, :active, :inactive |
| need_pw_change     | Bool        | Read Only                         |
| confimation_key    | String(255) | Read Only                         |
| role_id            | Int         |                                   |


User - from Profile

| Field              | Type        | Details                           |
|------------------- |------------ |---------------------------------- |
| nickname           | String(255) | Required                          |
| first_name         | String(255) |                                   |
| last_name          | String(255) |                                   |
| avatar             | String(255) |                                   |
| universities       | Association | Many to Many                      |
| posts              | Association | One to Many                       |
| followers          | Association | Many to Many                      |
| following          | Association | Many to Many                      |
| comments           | Association | One to Many                       |

Unknowns: FollowedByMe, InappropriateByMe


User - from Profile Counter

| Field                     | Type | Details   |
|-------------------------- |----- |---------- |
| post_count                | Int  | Not Null  |
| comment_count             | Int  | Not Null  |
| inapropriate_avatar_count | Int  | Not Null  |


User - from Profile Settings

| Field               | Type  | Details                 |
|-------------------- |------ |------------------------ |
| send_emails         | Bool  |                         |
| send_notifications  | Bool  | Not Null                |
| notification_type   | Enum  | Type of pushes to send  |


User - from Student

| Field               | Type        | Details                 |
|-------------------- |------------ |------------------------ |
| banned              | Bool        |                         |
| graduation_year     | Bool        |                         |
| gender              | Enum        | :male, :female          |
| student_type        | Method      |                         |
| living_on_campus    | Bool        |                         |
| status              | String(400) |                         |
| device_tokens       | Association |                         |


User - from Student Counter

| Field           | Type  | Details |
|---------------- |------ |-------- |
| ban_count       | Int   |         |
| login_count     | Int   |         |
| logout_count    | Int   |         |
| follower_count  | Int   |         |
| following_count | Int   |         |








ActivityRecord.cs	0.9.3	5 months ago
Administrator.cs	0.9.3	5 months ago
Advertiser.cs	0.9.3	5 months ago
App.config	0.9.3	5 months ago
App.cs	0.9.3	5 months ago
Comment.cs	0.9.3	5 months ago
CommentCounter.cs	0.9.3	5 months ago
DeviceToken.cs	0.9.3	5 months ago
DomainName.cs	0.9.3	5 months ago
Editor.cs	0.9.3	5 months ago
Email.cs	0.9.3	5 months ago
Entity.cs	0.9.3	5 months ago
Follow.cs	0.9.3	5 months ago
HashTag.cs	0.9.3	5 months ago
ICounter.cs	0.9.3	5 months ago
IUniversityUser.cs	0.9.3	5 months ago
PersistentEntity.cs	0.9.3	5 months ago
Post.cs	0.9.3	5 months ago
PostCounter.cs	0.9.3	5 months ago
PostHashTag.cs	0.9.3	5 months ago
Privilege.cs	0.9.3	5 months ago
ProfileUniversity.cs	0.9.3	5 months ago
Role.cs	0.9.3	5 months ago
TimeDisappearingPost.cs	0.9.3	5 months ago
UUIDEntity.cs	0.9.3	5 months ago
University.cs	v. 0.9.7.2	5 months ago
UniversityCounter.cs	0.9.3	5 months ago

