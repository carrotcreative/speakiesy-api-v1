# Domain Name

- Source: [source/Domain/DomainName.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/DomainName.cs)
- Inherits: [source/Domain/UUIDEntity.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/UUIDEntity.cs)

| Field  	                | Type            | Required  | Other                                                 |
|-----------------------	|----------------	|----------	|------------------------------------------------------	|
| UUID                    | String(32)      |           |                                                       |
| Name                    | String(255)     | Yes       | [Index("IX_DomainNames_UniqueName", IsUnique = true)] |
| UniversityId            | Long            |           | [ForeignKey("University")]                            |
| University              | University      |           | [ForeignKey("UniversityId")]                          |
