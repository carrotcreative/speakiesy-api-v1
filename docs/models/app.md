# App

- Source: [source/Domain/App.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/App.cs)
- Inherits: [source/Domain/UUIDEntity.cs](https://github.com/carrot/speakiesy-web/blob/master/source/Domain/UUIDEntity.cs)

| Field  	                | Type          | Required  | Other                     |
|-----------------------	|--------------	|----------	|-------------------------	|
| UUID                    | String(32)    |           |                           |
| Name      	            | String(50)    | Yes      	|                   	      |
| Key                   	| String(255)   | Yes       |       	                  |
| AppType                 | AppType       | Yes       |                         	|
| DeviceTokens            | ICollection   | Yes       |                         	|
